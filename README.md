# DDR a method to predict drug target interactions using multiple similarities 


#### Dependencies: 
- Python 2.7
- numpy
- Scikitlearn 


#### Input format and files:
- DDR expects all network files to in form of adjacency list file. 
- For relation files DDR expect a tuple of drug and target in each line
- For similarity files DDR expects a tuple of drug (target) and drug (target) and their similarity

#### Usage 
usage: DDR.py [-h] --interaction R_FILE --DSimilarity D_SIM_FILE --TSimilarity
              T_SIM_FILE --outfile OUT_FILE [--no_of_splits NO_OF_SPLITS]
              [--K K] [--K_SNF K_SNF] [--T_SNF T_SNF] [--N NO_OF_TREES]
              [--s SPLIT]

DDR a method to predict drug target interactions

optional arguments:
  -h, --help            show this help message and exit
  --no_of_splits NO_OF_SPLITS
                        Number of parts to split unkown interactions. Default:
                        10
  --K K                 Number of nearest neighbors for drugs and targets
                        neigborhood. Default: 5
  --K_SNF K_SNF         Number of neighbors similarity fusion. Default: 3
  --T_SNF T_SNF         Number of iteration for similarity fusion. Default: 10
  --N NO_OF_TREES       Number trees for random forest. Default: 100
  --s SPLIT             Split critera for random forest trees. Default: gini

required named arguments:
  --interaction R_FILE  Name of the file containg drug target interaction
                        tuples
  --DSimilarity D_SIM_FILE
                        Name of the file containg drug similarties file names
  --TSimilarity T_SIM_FILE
                        Name of the file containg target similarties file
                        names
  --outfile OUT_FILE    Output file to write predictions
  
#### Contact
 - Vladimir Bajic (vladimir-dot-bajic-at-kaust-dot-edu-dot-sa)
 - Rawan Olayan (rawan-dot-olayan-at-kaust-dot-edu-dot-sa))
