# Similarity Selection and Reduction

This directory contains scripts for similarity selection. 
#### Dependencies: 
- Python 2.7
- numpy
- Scikitlearn 


#### Input format
Similarity selection scripts expect input in form of similarity matrix


#### Command:
python Similarity_Selection.py <similarities_file_name> <Similarity_cutoff> <Entropy_cutoff>