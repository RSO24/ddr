'''
*************************************************************************
Copyright (c) 2017, Rawan Olayan

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************
'''
import sklearn
from sklearn.metrics.pairwise import euclidean_distances
import os,sys,math
import numpy as np
#------------------------------------------------------------------------------------------------- 
def read_Sim_Calc_Dist(f1,f2,cutoff):
        cutoff = float(cutoff)

        A = np.loadtxt(f1, delimiter=',')
        B = np.loadtxt(f2, delimiter=',')

        for i in range(len(A)):
                for j in range(len(A[i])):
                        v= A[i][j]
                        if v < cutoff and i != j:
                                A[i][j]=0

        for i in range(len(B)):
                for j in range(len(B[i])):
                        v= B[i][j]
                        if v < cutoff and i != j:
                                B[i][j]=0

        A_vec = A.flatten()
        B_vec = B.flatten()
        dist = sklearn.metrics.pairwise.pairwise_distances(A_vec, B_vec, metric='euclidean')
        return dist[0][0]

#---------------------------------------------------------------------------------------------------
def read_Sim_Calc_Entropy(fname,cutoff):
        entropy_exclude_zero_sumRow=[]
        max_entropy=0.0
        #print 'fname',fname
        cutoff=float(cutoff)
        entropy=[]
        
        small_number= 1*pow(10,-16)
        arr = np.loadtxt(fname, delimiter=',')
        np.fill_diagonal(arr,0)
        #arr +=small_number
        #print 'arr',arr


        row,col = arr.shape
        aIndices_nonZero=[]
        max_entropy = float(math.log(row,2))

        for i in range(row):
                for j in range(col):
                        if arr[i][j]<cutoff:
                                arr[i][j]=0
        
        for i in range(len(arr)):
                row_sum =arr[i].sum() 
                row_entropy=0

                if row_sum == 0:
                        entropy.append(0)
                        
                if row_sum > 0:
                        aIndices_nonZero.append(i)
                        arr[i] +=small_number 
                        row_sum = arr[i].sum()
			for j in range(len(arr[i])):
                                v= arr[i][j]
                                cell_edited = (v)/row_sum
                                #print 'cell_edited',cell_edited
                                row_entropy= row_entropy+(cell_edited * math.log(cell_edited,2))
                                 #print 'row_entropy',row_entropy
                        row_entropy =row_entropy*-1
                        entropy.append(row_entropy)

        for x in aIndices_nonZero:            
                entropy_exclude_zero_sumRow.append(entropy[x])
        
        #print 'entropy',entropy
        return np.mean(entropy),np.mean(entropy_exclude_zero_sumRow),round(max_entropy,2)
#---------------------------------------------------------------------------------------------------
def removeRedundancy(ranked_entropy_simType,all_euclideanDist_Sim):
        flT = 0.6
        m = 0
        iMEnd = len(ranked_entropy_simType)
        while m < iMEnd:
                print m,iMEnd
                A_simType = ranked_entropy_simType[m]
                n = m+1
                iNEnd = len(ranked_entropy_simType)
                while n < iNEnd:
                        B_simType = ranked_entropy_simType[n]

                        if A_simType+','+B_simType in all_euclideanDist_Sim:
                                key=A_simType+','+B_simType
                        if B_simType+','+A_simType in all_euclideanDist_Sim:
                                key=B_simType+','+A_simType

                        flMax = all_euclideanDist_Sim[key]
                        if flMax > flT:
                                #oMC.deleteMotif(sMotB)
                                del ranked_entropy_simType[n]
                        else:
                                n += 1
                        iNEnd = len(ranked_entropy_simType)
                m += 1
                iMEnd = len(ranked_entropy_simType)
        print 'ranked_entropy_simType', ranked_entropy_simType

        return ranked_entropy_simType
#---------------------------------------------------------------------------------------------------

if __name__ == '__main__':
        outFile='AllCutoff_Entropy_excluded_zero_sumRow_removedRedundancy_06_'
        entropyType_value={}
        entropy_value =[]
        entropy_simType=[]
        sim_entropy_ranked=[]
        entropy_type_value=[]
        output= './'
        
        filename=sys.argv[1]
        cutoff=float(sys.argv[2])
        entropy_percenatge=float(sys.argv[3])

        sim_files = open(filename, 'r')
        for line in sim_files: 
                line=line.rstrip()
                entropy,entropy_exclude_zero_sumRow,max_entropy =read_Sim_Calc_Entropy(line,cutoff)
                
                #print 'entropy,entropy_exclude_zero_sumRow,SimType',entropy,entropy_exclude_zero_sumRow,line
                #print 'max_entropy',max_entropy
                #print 'max/2',0.7 *max_entropy
                entropy_type_value.append(line+'\t'+str(entropy_exclude_zero_sumRow))
                entropyType_value[line]=entropy_exclude_zero_sumRow
                #print 'line :: entropy_exclude_zero_sumRow', line,entropy_exclude_zero_sumRow
                #remove entropy > max/2 = log(no.drugs,2)/2 and rank
                if entropy_exclude_zero_sumRow <= entropy_percenatge *max_entropy :
                        #print 'correct',line,entropy_exclude_zero_sumRow
                        entropy_value.append(entropy_exclude_zero_sumRow)
                        entropy_simType.append(line)
        
        ranked_entropy_simType = [ str(x) for (y,x) in sorted(zip(entropy_value,entropy_simType))] 
        print 'ranked_entropy_simType',ranked_entropy_simType
        #print 'entropyType_value',entropyType_value

        with open(output+"Entropy_excluded_zero_sumRow_Cutoff_"+str(cutoff)+"_"+filename , 'w') as f:
                f.write('\n'.join(entropy_type_value))
        f.close()
        
        '''
        with open(output+"Entropy_lessThanMax_Cutoff_"+str(cutoff)+"_"+filename , 'w') as f:
                for a,b in zip(entropy_simType,entropy_value):
                        print 'a,b',a,b
                        f.write(a+'\t'+str(b)+'\n')
        f.close()
        '''
        
        #calculating distnace
        all_euclideanDist_Sim={}
        fo = open(filename, 'r')
        lines=fo.readlines()
        length=len(lines)
        for i in range(length-1):
                f1=lines[i].rstrip()
                for j in range(i+1,length):
                        f2=lines[j].rstrip()
                        euclideanDist =  read_Sim_Calc_Dist(f1,f2,cutoff)
                        all_euclideanDist_Sim[f1+','+f2] =1.0/(euclideanDist+1.0)

        #print 'all_euclideanDist_Sim',sorted([(value,key) for (key,value) in all_euclideanDist_Sim.items()])
        with open(output+"euclideanDist_Sim_Entropy_excluded_zero_sumRow_Cutoff_"+str(cutoff)+"_"+filename , 'w') as f:
                for a,b in all_euclideanDist_Sim.items():
                        f.write(a+'\t'+str(b)+'\n')
        f.close()
        
        ranked_entropy_simType = removeRedundancy(ranked_entropy_simType,all_euclideanDist_Sim)
        with open(output+outFile+filename , 'a') as f:
                for a in ranked_entropy_simType:
                        f.write('cutoff:'+str(cutoff)+'\t'+'SimType:'+a+'\t'+'EntropyValue:'+str(entropyType_value[a])+'\n')
        f.close()
        
