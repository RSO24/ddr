'''
*************************************************************************
Copyright (c) 2017, Rawan Olayan

>>> SOURCE LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************
'''
import numpy as np
import pandas as pd
import collections
import csv
import os,sys
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
def run_SNF(entropy_values,entropy_simTypes,k_highestEntropym):
    W=[]
    run_SNF(entropy_values,entropy_simTypes,k_highestEntropym)
    high_entropy_values =[a for (a,b) in sorted(zip(entropy_values,entropy_simTypes))]
    high_entropy_simTypes= [b for (a,b) in sorted(zip(entropy_values,entropy_simTypes))]
    high_entropy_simTypes=high_entropy_simTypes[:k_highestEntropym]
    Wall=[]
    for sim_file in high_entropy_simTypes:
        w = np.loadtxt(sim_file, dtype='%.4f', delimiter='\t')
        Wall.append(w)
    W =SNF(Wall,3,2,ALPHA=1)
    return W
#------------------------------------------------------------------------------------------------------------------------------------


def read_Sim_Calc_Entropy(fname,cutoff):
    print 'fname',fname
    entropy=[]
    
    small_number= 1*pow(10,-16)
    arr = np.loadtxt(fname, delimiter='\t')
    np.fill_diagonal(arr,0)
    arr +=small_number
    #print 'arr',arr
    
    for i in range(len(arr)):
        row_sum =arr[i].sum() 
        row_entropy=0
        for j in range(len(arr[i])):
            v= arr[i][j]
            if v < cutoff and i != j:
                v=small_number
            cell_edited = (v)/row_sum
            #print 'cell_edited',cell_edited
            row_entropy= row_entropy+(cell_edited * math.log(cell_edited,2))
            #print 'row_entropy',row_entropy
        row_entropy =row_entropy*-1
        entropy.append(row_entropy)
    
    return np.mean(entropy)
#------------------------------------------------------------------------------------------------------------------------------------
def write_2_file_dict(fname,dict_data):
    with open(fname, 'w') as f:
        for k,v in dict_data.items():
            f.write(str(k)+','+str(v)+'\n')
    f.close()        
#------------------------------------------------------------------------------------------------------------------------------------    
def get_R_D_T(R_file):
    print 'get_R_D_T'

    D =[]
    T = []
    R = []

    oF = open(R_file)
    for line in oF.readlines():
        d,t = line.strip().split()
        R.append(d+','+t)
        D.append(d)
        T.append(t)

    D_set =set(D)
    T_set =set(T)
    print 'D,T,R',len(D_set),len(T_set),len(R)

    return R,list(D_set),list(T_set)
#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def SimLine_2_SimMat_D(name, dDs,cutoff):
    #read regular sim_files
    D_Edited_Sim_cutoff_files =[]

    fname = open(name)
    print 'name',fname
    pairs_Simvalue = {}
    for sim in fname.readlines():
        pairs_Simvalue[sim.split('\t')[0]+','+sim.split('\t')[1]]=sim.split('\t')[2]

    adj = np.zeros((len(dDs.keys()), len(dDs.keys())))
    np.fill_diagonal(adj, 1)
    for pair,sim in pairs_Simvalue.items():
        v = pairs_Simvalue[pair]
        i = dDs[pair.split(',')[0]]
        j = dDs[pair.split(',')[1]]
        if v < cutoff:
            v= 0.0001
        adj[i][j] = v
        adj[j][i] = v
    adj[adj < cutoff] = '0.0001' 
    dsim_fname_base=os.path.splitext(name)[0]
    file_name = dsim_fname_base+'_'+str(cutoff)+'_mat.txt'
    D_Edited_Sim_cutoff_files.append(file_name)
    print file_name
 
    np.savetxt(file_name,adj,fmt='%.4f')
    return D_Edited_Sim_cutoff_files

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def SimLine_2_SimMat_T(name, dTs,cutoff):

    T_Edited_Sim_cutoff_files =[]
    #read regular sim_files
    fname = open(name)
    print 'name',fname
    pairs_Simvalue = {}
    for sim in fname.readlines():
        pairs_Simvalue[sim.split('\t')[0]+','+sim.split('\t')[1]]=sim.split('\t')[2]

    adj = np.zeros((len(dTs.keys()), len(dTs.keys())))
    np.fill_diagonal(adj, 1)
    for pair,sim in pairs_Simvalue.items():
        v = pairs_Simvalue[pair]
        i = dTs[pair.split(',')[0]]
        j = dTs[pair.split(',')[1]]
        adj[i][j] = v
        adj[j][i] = v
    adj[adj < cutoff] = '0.0001'
    tsim_fname_base=os.path.splitext(name)[0]
    file_name = tsim_fname_base+'_'+str(cutoff)+'_mat.txt'
    T_Edited_Sim_cutoff_files.append(file_name)
    print file_name
    
    np.savetxt(file_name,adj,fmt='%.4f')
    return T_Edited_Sim_cutoff_files
#============================================================================================================================================================================
def SimLine2Mat_run(db_name,R_file,D_simLine_file,T_simLine_file,cutoff):
   

    R, d, t = get_R_D_T(R_file)
    dDs = dict([(x, i) for i, x in enumerate(d)])
    dTs = dict([(x, i) for i, x in enumerate(t)])
    print 'd',len(dDs)
    print 't',len(dTs)

    write_2_file_dict(db_name+'_dDs.txt',dDs)
    write_2_file_dict(db_name+'_dTs.txt',dTs)

    # d gip sim file
    d_file = open(D_simLine_file, 'r')
    for line in d_file: 
        line=line.strip()
        print 'd_sim_file:',line
        SimLine_2_SimMat_D(line, dDs,cutoff)

    t_file = open(T_simLine_file, 'r')
    for line in t_file: 
        line=line.strip()
        print 't_sim_file:',line
        SimLine_2_SimMat_T(line, dTs,cutoff)
    


#============================================================================================================================================================================
if __name__ == '__main__':
    db_name = sys.argv[ 1]
    R_file = sys.argv[ 2]
    D_simLine_file = sys.argv[3 ]
    T_simLine_file = sys.argv[ 4]
    cutoff = float(sys.argv[ 5])
    #-----------------------------------------CUTOFF SIM------------------------------------------------
    D_Edited_Sim_cutoff_files = SimLine_2_SimMat_D(name, dDs,cutoff,D_Edited_Sim_cutoff_files)
    print 'D_Edited_Sim_cutoff_files',D_Edited_Sim_cutoff_files
    T_Edited_Sim_cutoff_files = SimLine_2_SimMat_T(name, dTs,cutoff,T_Edited_Sim_cutoff_files)
    print 'T_Edited_Sim_cutoff_files',T_Edited_Sim_cutoff_files
    #------------------------------------------ENTROPY--------------------------------------------------
    D_entropy_values=[]
    D_entropy_simTypes=[]
    for line in D_Edited_Sim_cutoff_files: 
        entropy =read_Sim_Calc_Entropy(line,cutoff)
        print 'entropy',entropy
        D_entropy_simTypes.append(line)
        D_entropy_values.append(str(entropy))
    with open(db_name+"_D_Entropy_Cutoff"+str(cutoff)+".txt" , 'w') as f:
        for a,b in zip(D_entropy_simTypes,D_entropy_values):
            f.write(a+'\t'+b+'\n')
    f.close()
                    #------------------------------------------
    T_entropy_values=[]
    T_entropy_simTypes=[]
    for line in T_Edited_Sim_cutoff_files: 
        entropy =read_Sim_Calc_Entropy(line,cutoff)
        print 'entropy',entropy
        T_entropy_simTypes.append(line)
        T_entropy_values.append(str(entropy))
    with open(db_name+"_T_Entropy_Cutoff"+str(cutoff)+".txt" , 'w') as f:
        for a,b in zip(T_entropy_simTypes,T_entropy_values):
            f.write(a+'\t'+b+'\n')
    f.close()
    #-------------------------------------------SNF-----------------------------------------------------
    k_highestEntropym=3
    file_name='_KhighEntropy_'+str(k_highestEntropym)+'_Cutoff_'+str(cutoff)+".txt"
    D_Wall = run_SNF(D_entropy_values,D_entropy_simTypes,k_highestEntropym)
    np.savetxt('D'+file_name,D_Wall,fmt='%.4f')
    T_Wall = run_SNF(T_entropy_values,T_entropy_simTypes,k_highestEntropym)
    np.savetxt('T'+file_name,T_Wall,fmt='%.4f')
    